const Generator = require('../../abstractGenerator');
const {randomise, unique, randomElement, sumDigits} = require('../../helpers');
const {digits} = require('../../sets');
const handleParams = require('../../handleParams');

// specs: https://download.elster.de/download/schnittstellen/Pruefung_der_Steuer_und_Steueridentifikatsnummer.pdf

module.exports = class StNr extends Generator {

    static get regions() {
        return {
            BW: {code: 28, prefix: '',  officeDigits: 2, districtDigits: 3, unitDigits: 4, sep1: '',  sep2: '/', checksumAlg: '2',   checksumFactors: {summands: [0, 0, 9, 8, 0, 7, 6, 5, 4, 3, 2, 1], factors: [0, 0, 512, 256, 0, 128, 64, 32, 16, 8, 4, 2]}},
            BY: {code: 9,  prefix: '',  officeDigits: 3, districtDigits: 3, unitDigits: 4, sep1: '/', sep2: '/', checksumAlg: '11',  checksumFactors: [0, 5, 4, 3, 0, 2, 7, 6, 5, 4, 3, 2]},
            BE: {code: 11, prefix: '',  officeDigits: 2, districtDigits: 3, unitDigits: 4, sep1: '/', sep2: '/', checksumAlg: '11',  checksumFactors: {A: [0, 0, 0, 0, 0, 7, 6, 5, 8, 4, 3, 2], B: [0, 0, 2, 9, 0, 8, 7, 6, 5, 4, 3, 2]}},
            BB: {code: 30, prefix: '0', officeDigits: 2, districtDigits: 3, unitDigits: 4, sep1: '/', sep2: '/', checksumAlg: '11',  checksumFactors: [0, 5, 4, 3, 0, 2, 7, 6, 5, 4, 3, 2]},
            HB: {code: 24, prefix: '',  officeDigits: 2, districtDigits: 3, unitDigits: 4, sep1: ' ', sep2: ' ', checksumAlg: '11',  checksumFactors: [0, 0, 4, 3, 0, 2, 7, 6, 5, 4, 3, 2]},
            HH: {code: 22, prefix: '',  officeDigits: 2, districtDigits: 3, unitDigits: 4, sep1: '/', sep2: '/', checksumAlg: '11',  checksumFactors: [0, 0, 4, 3, 0, 2, 7, 6, 5, 4, 3, 2]},
            HE: {code: 26, prefix: '0', officeDigits: 2, districtDigits: 3, unitDigits: 4, sep1: ' ', sep2: ' ', checksumAlg: '2',   checksumFactors: {summands: [0, 0, 9, 8, 0, 7, 6, 5, 4, 3, 2, 1], factors: [0, 0, 512, 256, 0, 128, 64, 32, 16, 8, 4, 2]}},
            MV: {code: 40, prefix: '0', officeDigits: 2, districtDigits: 3, unitDigits: 4, sep1: '/', sep2: '/', checksumAlg: '11',  checksumFactors: [0, 5, 4, 3, 0, 2, 7, 6, 5, 4, 3, 2]},
            NI: {code: 23, prefix: '',  officeDigits: 2, districtDigits: 3, unitDigits: 4, sep1: '/', sep2: '/', checksumAlg: '11',  checksumFactors: [0, 0, 2, 9, 0, 8, 7, 6, 5, 4, 3, 2]},
            NW: {code: 5,  prefix: '',  officeDigits: 3, districtDigits: 4, unitDigits: 3, sep1: '/', sep2: '/', checksumAlg: '11',  checksumFactors: [0, 3, 2, 1, 0, 7, 6, 5, 4, 3, 2, 1]},
            RP: {code: 27, prefix: '',  officeDigits: 2, districtDigits: 3, unitDigits: 4, sep1: '/', sep2: '/', checksumAlg: '11M', checksumFactors: [0, 0, 1, 2, 0, 1, 2, 1, 2, 1, 2, 1]},
            SL: {code: 10, prefix: '0', officeDigits: 2, districtDigits: 3, unitDigits: 4, sep1: '/', sep2: '/', checksumAlg: '11',  checksumFactors: [0, 5, 4, 3, 0, 2, 7, 6, 5, 4, 3, 2]},
            SN: {code: 32, prefix: '2', officeDigits: 2, districtDigits: 3, unitDigits: 4, sep1: '/', sep2: '/', checksumAlg: '11',  checksumFactors: [0, 5, 4, 3, 0, 2, 7, 6, 5, 4, 3, 2]},
            ST: {code: 31, prefix: '1', officeDigits: 2, districtDigits: 3, unitDigits: 4, sep1: '/', sep2: '/', checksumAlg: '11',  checksumFactors: [0, 5, 4, 3, 0, 2, 7, 6, 5, 4, 3, 2]},
            SH: {code: 21, prefix: '',  officeDigits: 2, districtDigits: 3, unitDigits: 4, sep1: '/', sep2: '/', checksumAlg: '2',   checksumFactors: {summands: [0, 0, 9, 8, 0, 7, 6, 5, 4, 3, 2, 1], factors: [0, 0, 512, 256, 0, 128, 64, 32, 16, 8, 4, 2]}},
            TH: {code: 41, prefix: '1', officeDigits: 2, districtDigits: 3, unitDigits: 4, sep1: '/', sep2: '/', checksumAlg: '11',  checksumFactors: [0, 5, 4, 3, 0, 2, 7, 6, 5, 4, 3, 2]},
        };
    }

    params() {
        return {
            schema: {
                type: 'string',
                values: ['state', 'federal'],
                default: 'federal',
            },
            state: {
                type: 'string',
                values: Object.keys(StNr.regions),
                default: null,
            }
        };
    }

    generate(params = {}) {
        params = handleParams(this.params(), params);

        const state = params.state || randomElement(Object.keys(StNr.regions));
        const stateData = StNr.regions[state];

        const office = randomise(digits, stateData.officeDigits);
        const district = randomise(digits, stateData.districtDigits);
        const unit = randomise(digits, stateData.unitDigits);

        const stateFormat = `${stateData.prefix}${office}${stateData.sep1}${district}${stateData.sep2}${unit}`;
        const federalFormat = `${stateData.code}${office}0${district}${unit}`;

        const checksum = this._calculateChecksum(federalFormat, stateData.checksumAlg, stateData.checksumFactors);

        return (params.schema === 'federal' ? federalFormat : stateFormat) + checksum;
    }

    validate(value) {
        value = value.trim();

        const possibleStates = {};
        if (value.match(/^\d{13}$/g)) { // federal schema
            for (let state in StNr.regions) {
                if (StNr.regions.hasOwnProperty(state)) {
                    if (value.startsWith(StNr.regions[state].code)) {
                        possibleStates[state] = value;
                        break;
                    }
                }
            }
        } else {
            const m = value.match(/^([012]?)(\d{2,3})([\/ ]?)(\d{3,4})([\/ ])(\d{3,4})(\d)$/);
            if (!m) {
                throw new Error('format');
            }
            const [all, prefix, office, sep1, district, sep2, unit, checksum] = m;
            for (let state in StNr.regions) {
                if (StNr.regions.hasOwnProperty(state)) {
                    const stateData = StNr.regions[state];
                    let iPrefix = prefix;
                    let iOffice = office;
                    if (iPrefix !== '' && iOffice.length < stateData.officeDigits) {
                        iOffice = iPrefix + iOffice;
                        iPrefix = ''
                    }

                    if (iPrefix === stateData.prefix
                        && iOffice.length === stateData.officeDigits
                        && sep1 === stateData.sep1
                        && district.length === stateData.districtDigits
                        && sep2 === stateData.sep2
                        && unit.length === stateData.unitDigits
                    ) {
                        possibleStates[state] = `${stateData.code}${iOffice}0${district}${unit}${checksum}`;
                    }
                }
            }
        }

        if (Object.keys(possibleStates).length === 0) {
            throw new Error('region')
        }

        const validStates = [];
        for (let state in possibleStates) {
            if (possibleStates.hasOwnProperty(state)) {
                const number = possibleStates[state];
                const checksum = this._calculateChecksum(
                    number.substr(0, 12),
                    StNr.regions[state].checksumAlg,
                    StNr.regions[state].checksumFactors
                );

                if (checksum === parseInt(number[12])) {
                    validStates.push(state);
                }
            }
        }

        if (validStates.length === 0) {
            throw new Error('checksum')
        }

        return {
            regions: validStates.map(stateCode => ['DE', stateCode]),
        };
    }

    _calculateChecksum(digits, alg, factors) {
        switch (alg) {
            case '11M':
                return this._calculateChecksum11M(digits, factors);
            case '2':
                return this._calculateChecksum2(digits, factors);
            case '11':
                if (digits.substr(0, 2) === '11') { // BE
                    factors = factors[this._whichBerlin(digits)];
                }
                return this._calculateChecksum11(digits, factors);
            default:
                throw new Error('checksumAlg')
        }
    }

    _calculateChecksum11M(digits, factors) {
        let sum = 0;
        for (let i in digits) {
            if (digits.hasOwnProperty(i)) {
                let digit = digits[i] * factors[i];
                if (digit >= 10) {
                    digit = digit % 10 + 1;
                }
                sum += digit;
            }
        }

        return (10 - sum % 10) % 10;
    }

    _calculateChecksum2(digits, factors) {
        let sum = 0;
        for (let i in digits) {
            if (digits.hasOwnProperty(i)) {
                let digit = (parseInt(digits[i]) + factors.summands[i]) % 10 * factors.factors[i];

                while (digit >= 10) {
                    digit = sumDigits(digit);
                }
                sum += digit;
            }
        }

        return (10 - sum % 10) % 10;
    }

    _calculateChecksum11(digits, factors) {
        let sum = 0;
        for (let i in digits) {
            if (digits.hasOwnProperty(i)) {
                sum += digits[i] * factors[i];
            }
        }

        if (digits.substr(0, 1) === '5') { // NW
            return (sum % 11) % 10;
        }

        return ((11 - sum % 11) % 11) % 10;

        // it's unclear what to do, if checksum == 10... i reduce it to 0, but...
    }

    _whichBerlin(digits) {
        const office = parseInt(digits.substr(0, 4));
        const district = parseInt(digits.substr(5, 3));

        switch (office) {
            case 1113:
            case 1114:
            case 1117:
            case 1120:
            case 1121:
            case 1123:
            case 1124:
            case 1125:
            case 1140:
                return district >= 201 && district <= 693 ? 'B' : 'A';
            case 1116:
                return (district >= 1 && district <= 29) || (district >= 201 && district <= 693) ? 'B' : 'A';
            case 1119:
                return (district >= 201 && district <= 693) || district === 680 || district === 684 ? 'B' : 'A';
            case 1127:
            case 1129:
            case 1130:
                return 'A';
            case 1118:
            case 1131:
            case 1132:
            case 1133:
            case 1134:
            case 1135:
            case 1136:
            case 1137:
            case 1194:
            case 1195:
            case 1196:
            case 1197:
            case 1198:
                return 'B';
            default:
                return 'A'; // Finanzamt doesn't exist, but we don't validate them for other regions anyway, so let's just go with A
        }
    }
};
