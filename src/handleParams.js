const cast = function(type, value) {
    switch (type) {
        case 'int':
            return parseInt(value);
        case 'bool':
            return !!value;
        case 'string':
        default:
            return value === null ? '' : value + '';
    }
};

const validate = function(key, value, schema) {
    if (schema.values !== undefined && schema.values.indexOf(value) === -1) {
        throw new Error(`Param ${key} outside of allowed value set.`);
    }

    if (schema.min !== undefined && value < schema.min) {
        throw new Error(`Param ${key} cannot be lower than ${schema.min}`);
    }

    if (schema.max !== undefined && value > schema.max) {
        throw new Error(`Param ${key} cannot be higher than ${schema.max}`);
    }

    if (schema.pattern !== undefined && !(new RegExp(schema.pattern)).test(value)) {
        throw new Error(`Param ${key} doesn't match the requested format.`);
    }
};

const handleParams = function (schema, params) {
    const out = {};

    for (let key in schema) {
        if (schema.hasOwnProperty(key)) {
            if (key in params) {
                const value = cast(
                    schema[key].type,
                    params[key] || schema[key].default || null
                );

                if (value) {
                    validate(key, value, schema[key]);
                }

                out[key] = value;
            } else {
                out[key] = cast(
                    schema[key].type,
                    schema[key].default || null
                );
            }
        }
    }

    return out;
};

module.exports = handleParams;
